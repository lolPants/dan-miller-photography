import React, { Component } from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'

import Home from './Home.jsx'
import { Animals, Events, People, Scenery } from './PhotoPages.jsx'
import Contact from './Contact.jsx'

import 'bootstrap/dist/css/bootstrap.css'
import '../styles/App.css'

class App extends Component {
  render () {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={ Home } />
          <Route path='/animals' component={ Animals } />
          <Route path='/events' component={ Events } />
          <Route path='/people' component={ People } />
          <Route path='/scenery' component={ Scenery } />
          <Route path='/contact' component={ Contact } />
        </Switch>
      </Router>
    )
  }
}

export default App
