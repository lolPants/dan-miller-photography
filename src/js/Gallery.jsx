import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-bootstrap'
import Lightbox from 'react-images'

import chunkArray from './dist/chunkArray.js'

class Gallery extends Component {
  constructor (props) {
    super(props)

    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
    }

    this.closeLightbox = this.closeLightbox.bind(this)
    this.gotoNext = this.gotoNext.bind(this)
    this.gotoPrev = this.gotoPrev.bind(this)
    this.gotoImage = this.gotoImage.bind(this)
    this.openLightbox = this.openLightbox.bind(this)
  }

  openLightbox (i, e) {
    e.preventDefault()
    this.setState({
      currentImage: i,
      lightboxIsOpen: true,
    })
  }
  closeLightbox () {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    })
  }
  gotoPrev () { this.setState({ currentImage: this.state.currentImage - 1 }) }
  gotoNext () { this.setState({ currentImage: this.state.currentImage + 1 }) }
  gotoImage (i) { this.setState({ currentImage: i }) }

  render () {
    let chunked = chunkArray(this.props.images, 4)
    let map = this.props.images.map(image => ({ src: image }))
    return (
      <div>
        <Lightbox
          images={ map }
          isOpen={ this.state.lightboxIsOpen }
          onClickPrev={ this.gotoPrev }
          onClickNext={ this.gotoNext }
          onClose={ this.closeLightbox }
          currentImage={ this.state.currentImage }
        />
        {
          chunked.map((chunk, i) =>
            <Row key={ i }>
              {
                chunk.map((img, j) =>
                  <Col sm={3} key={ j }>
                    <a href='' onClick={ e => this.openLightbox((4 * i) + j, e) }>
                      <img
                        src={ img }
                        alt=''
                        className='gallery-img'
                      />
                    </a>
                  </Col>
                )
              }
            </Row>
          )
        }
      </div>
    )
  }
}

Gallery.propTypes = {
  title: PropTypes.string,
  images: PropTypes.array,
}

export default Gallery
