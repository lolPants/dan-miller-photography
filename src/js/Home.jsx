import React from 'react'
import { Helmet } from 'react-helmet'

import NavBar from './NavBar.jsx'

class Home extends React.Component {
  render () {
    return (
      <div>
        <Helmet>
          <title>Dan Miller | Photography</title>
          <style>{ 'body { overflow:hidden; }' }</style>
        </Helmet>
        <NavBar transparent />
        <div className='landing-bg' />
        <div className='container landing'>
          <h1 className='landing'>Dan Miller</h1>
          <h2 className='landing'>Photography</h2>
        </div>
      </div>
    )
  }
}

export default Home
