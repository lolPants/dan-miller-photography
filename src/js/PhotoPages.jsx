import React from 'react'
import GalleryPage from './GalleryPage.jsx'

const importAll = r => r.keys().map(r)

const AnimalsGallery = importAll(require.context('../images/animals', false, /\.(png|jpe?g|svg)$/i))
const EventsGallery = importAll(require.context('../images/events', false, /\.(png|jpe?g|svg)$/i))
const PeopleGallery = importAll(require.context('../images/people', false, /\.(png|jpe?g|svg)$/i))
const SceneryGallery = importAll(require.context('../images/scenery', false, /\.(png|jpe?g|svg)$/i))

const Animals = () => <GalleryPage title='Animals' images={ AnimalsGallery } />
const Events = () => <GalleryPage title='Events' images={ EventsGallery } />
const People = () => <GalleryPage title='People' images={ PeopleGallery } />
const Scenery = () => <GalleryPage title='Scenery' images={ SceneryGallery } />

export { Animals, Events, People, Scenery }
