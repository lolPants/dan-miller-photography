import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import PropTypes from 'prop-types'

import NavBar from './NavBar.jsx'
import Gallery from './Gallery.jsx'
import Footer from './Footer.jsx'

class GalleryPage extends Component {
  render () {
    return (
      <div>
        <Helmet>
          <title>Dan Miller | { this.props.title }</title>
        </Helmet>
        <NavBar />
        <div className='container main'>
          <h1>{ this.props.title }</h1>
          <Gallery title={ this.props.title } images={ this.props.images } />
        </div>
        <Footer />
      </div>
    )
  }
}

GalleryPage.propTypes = {
  title: PropTypes.string,
  images: PropTypes.array,
}

export default GalleryPage
