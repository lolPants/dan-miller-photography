import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Navbar, Nav, NavItem } from 'react-bootstrap'

import NavLogo from '../images/logo.png'

class NavBar extends Component {
  handleURI () {
    window.location.href = this.href
  }

  handleURL () {
    window.open(this.href, '_blank')
  }

  render () {
    let styleOverride
    if (this.props.transparent) styleOverride = 'default navbar-purple navbar-transparent navbar-fixed-top'
    else styleOverride = 'default navbar-purple navbar-fixed-top'

    return (
      <Navbar bsStyle={ styleOverride } collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <a href='#/'>
              <img
                src={ NavLogo }
                alt='Navbar Logo'
                className='navbar-img'
              />
            </a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavItem href='#/animals' onSelect={ this.handleURI }>Animals</NavItem>
            <NavItem href='#/events' onSelect={ this.handleURI }>Events</NavItem>
            <NavItem href='#/people' onSelect={ this.handleURI }>People</NavItem>
            <NavItem href='#/scenery' onSelect={ this.handleURI }>Scenery</NavItem>
            <NavItem href='#/contact' onSelect={ this.handleURI }>Contact</NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

NavBar.propTypes = {
  transparent: PropTypes.bool,
}

export default NavBar
