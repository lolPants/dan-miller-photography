import React from 'react'

const Footer = () => <footer>
  <div className='container'>
    <p>&copy; { new Date().getFullYear() } Dan Miller. All Rights Reserved.</p>
  </div>
</footer>

export default Footer
