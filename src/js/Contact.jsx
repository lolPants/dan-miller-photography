import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import { Row, Col } from 'react-bootstrap'

import NavBar from './NavBar.jsx'
import Footer from './Footer.jsx'

import ContactImage from '../images/contact.jpg'

class Contact extends Component {
  render () {
    return (
      <div>
        <Helmet>
          <title>Dan Miller | Contact</title>
        </Helmet>
        <NavBar />
        <div className='container main'>
          <h1>Contact</h1>
          <Row>
            <Col sm={ 4 }>
              <img
                src={ ContactImage }
                alt='Contact Profile'
                className='profile-img'
              />
            </Col>

            <Col sm={ 8 }>
              <h2>Dan Miller</h2>
              <p className='subtitle'>Freelance Photographer</p>

              <p>
                19-year-old amateur photographer based in the Lake District<br />
                Want an event, people, or pets photographed?<br />
                Contact me <a href='mailto:danmillermail@aol.com' target='_blank' rel='noopener noreferrer'>here</a> for details and pricing<br />
                You’ll receive a USB flash drive as well as an online link containing your photographs
              </p>

              <p>Instagram: <a href='https://www.instagram.com/danlmiller/' target='_blank' rel='noopener noreferrer'>https://www.instagram.com/danlmiller/</a></p>

              <h4>Equipment:</h4>
              <ul>
                <li>Nikon D3400</li>
                <li>iPhone 7 Plus</li>
              </ul>

              <h4>Contributors:</h4>
              <ul>
                <li>Anna Merrell</li>
                <li>Ella Clarke</li>
                <li>Sue Lomas</li>
              </ul>
            </Col>
          </Row>
        </div>
        <Footer />
      </div>
    )
  }
}

export default Contact
