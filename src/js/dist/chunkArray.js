/**
 * Split an array into chunks
 * @param {Array} arr - Source Array
 * @param {number} size - Length of each chunk
 * @returns {Array}
 */
const chunkArray = (arr, size) => arr.map((o, i) => i % size === 0 ? arr.slice(i, i + size) : null).filter(i => i)

export default chunkArray
