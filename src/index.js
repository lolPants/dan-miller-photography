import React from 'react'
import ReactDOM from 'react-dom'
import App from './js/App.jsx'
import './js/dist/watermark'
import registerServiceWorker from './js/service/registerServiceWorker'

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()
